import {Component, OnInit} from "@angular/core";
import {MaterialModule, MdSelectModule} from '@angular/material';
import {BookingService} from "../../services/booking.service";


@Component({
    selector: 'booking',
    templateUrl: 'booking.component.html',
    styleUrls: ['booking.scss']
})

export class BookingComponent implements OnInit{
    numberOfPeople;
    selectedValue: string;
    isActive:boolean;
    showTest:boolean;

    ngOnInit(): void {
        this.setPeople();
    }
    constructor(private bookingService:BookingService){
        this.numberOfPeople = [
            {value: 1, viewValue: '1 person'}
        ];
        this.isActive = false;
        this.showTest = true;
    }
    public bookTrip(){
        console.log("booking", this.bookingService.getBooking() );

    }
    public statusBla(status: boolean){
        console.log("this is new status ", status);
    }
    public setPeople():void{
        for(let i =2; i<20; i++){
            let obj = {
               value : i,
               viewValue: i + ' people'
            };
            this.numberOfPeople.push(obj);
        }

    }




}
