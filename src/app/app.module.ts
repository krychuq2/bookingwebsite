import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {Login} from "../components/login/login.component";
import {Register} from "../components/register/register.component";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {UserService} from "../services/user.service";
import {HttpModule} from "@angular/http";
import {User} from "../models/user.class";
import {HeaderComponent} from "../components/header/header.component";
import {HomeComponent} from "../pages/home/home.component";
import {RouterModule} from "@angular/router";
import {BookingComponent} from "../components/booking/booking.component";
import {MaterialModule, MdDatepicker, MdDatepickerModule, MdNativeDateModule, MdSelectModule} from '@angular/material';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NumberOfPeopleComponent} from "../components/numberOfPeople/numberOfPeople.component";
import {TimeSelectionComponent} from "../components/time-selection/time-selection.component";
import {Booking} from "../models/booking.class";
import {BookingService} from "../services/booking.service";


@NgModule({
  declarations: [
    AppComponent,
    Login,
    Register,
      HeaderComponent,
      HomeComponent,
      BookingComponent,
      NumberOfPeopleComponent,
      TimeSelectionComponent

  ],
  imports: [
      BrowserModule,
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      HttpModule,
      MdSelectModule,
      MdDatepickerModule,
      MdNativeDateModule,
      BrowserAnimationsModule,
      RouterModule.forRoot([
          {
              path: '',
              component: HomeComponent
          }
      ])
  ],
  providers: [FormBuilder, UserService, User, HeaderComponent, Booking, BookingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
